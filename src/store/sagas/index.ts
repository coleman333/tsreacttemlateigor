import { all } from 'redux-saga/effects';

// tslint:disable: import-name
import rootPaginationSaga from './pagination';
import rootStatisticSaga from './statistic';
import rootEquipmentSaga from './equipment';
import rootInventorySaga from './inventory';
import rootWarehouseSaga from './warehouse';
// tslint:enable: import-name

export default function* rootSaga() {
  yield all([
    rootPaginationSaga(),
    rootStatisticSaga(),
    rootEquipmentSaga(),
    rootInventorySaga(),
    rootWarehouseSaga(),
  ]);
}
