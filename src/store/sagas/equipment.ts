import axios from 'axios';
axios.defaults.baseURL = process.env.BASE_URL;

import {
  put,
  takeEvery,
  all,
} from 'redux-saga/effects';

import { equipmentActionTypes } from '../types/actionTypes';
import equipment from '../../services/equipment';
import { TableItems, PageOptions } from '../../store/types/table';

type Props = {
  type: string,
  filters: TableItems,
  pageOptions: PageOptions,
};

function* getEquipmentSortedWorker(props: Props) {
  const { filters, pageOptions } = props;

  yield put({
    type: equipmentActionTypes.getSorted,
    payload: { count: null, status: 'fetching' },
  });

  const data = yield equipment.getSorted(filters, pageOptions);

  (!data.status || data.status !== "error")
    ?  yield put({
      type: equipmentActionTypes.getSorted,
      payload: {
        data: data.data.allEquipment,
        status: 'fetched',
      },
    })
    : yield put({
      type: equipmentActionTypes.getSorted,
      payload: { data: null, status: 'error' },
    });
}

function* watchGetEquipmentSorted() {
  yield takeEvery(
    equipmentActionTypes.getSortedAsync,
    getEquipmentSortedWorker,
  );
}

export default function* rootEquipmentSaga() {
  yield all([
    watchGetEquipmentSorted(),
  ]);
}
