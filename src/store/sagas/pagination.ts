import axios from 'axios';
axios.defaults.baseURL = process.env.BASE_URL;

import {
  put,
  takeEvery,
  all,
} from 'redux-saga/effects';

import equipment from '../../services/equipment';
import inventory from '../../services/inventory';

import { pagesActionTypes } from '../types/actionTypes';

// Get Distinct Equipment Count
function* getEquipmentDistinctWorker() {
  yield put({
    type: pagesActionTypes.getEquipmentDistinct,
    payload: { count: null, status: 'fetching' },
  });

  const data = yield equipment.getEquipmentCountDistinct();

  (!data.status || data.status !== "error")
  ? yield put({
    type: pagesActionTypes.getEquipmentDistinct,
    payload: { count: data.data.equipmentCount, status: 'fetched' },
  })
  : yield put({
    type: pagesActionTypes.getEquipmentDistinct,
    payload: { count: null, status: 'error' },
  });
}

function* watchGetEquipmentDistinctCount() {
  yield takeEvery(pagesActionTypes.getEquipmentDistinctAsync, getEquipmentDistinctWorker);
}

// Get Distinct Inventory Count
function* getInventoryDistinctWorker() {
  yield put({
    type: pagesActionTypes.getInventoryDistinct,
    payload: { count: null, status: 'fetching' },
  });

  const data = yield inventory.getDistinctCount();

  (!data.status || data.status !== "error")
    ? yield put({
      type: pagesActionTypes.getInventoryDistinct,
      payload: { count: data.data.totalInventory, status: 'fetched' },
    })
    : yield put({
      type: pagesActionTypes.getInventoryDistinct,
      payload: { count: null, status: 'error' },
    });
}

function* watchGetInventoryDistinctCount() {
  yield takeEvery(pagesActionTypes.getInventoryDistinctAsync, getInventoryDistinctWorker);
}

export default function* rootPaginationSaga() {
  yield all([
    watchGetEquipmentDistinctCount(),
    watchGetInventoryDistinctCount(),
  ]);
}
