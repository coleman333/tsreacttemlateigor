import axios from 'axios';
axios.defaults.baseURL = process.env.BASE_URL;

import {
  takeEvery,
  all,
  put,
} from 'redux-saga/effects';

import { warehouseActionTypes } from '../types/actionTypes';
// tslint:disable-next-line: import-name
import warehouseService from '../../services/warehouse';

function* getAllWarehouseWorker() {
  yield put({
    type: warehouseActionTypes.getAll,
    payload: { data: null, status: 'fetching' },
  });

  const data = yield warehouseService.getAll();

  (!data.status || data.status !== "error")
    ? yield put({
      type: warehouseActionTypes.getAll,
      payload: {
        data: data.data,
        status: 'fetched',
      },
    })
    : yield put({
      type: warehouseActionTypes.getAll,
      payload: {
        data: null,
        status: data.status,
      },
    });
}

function* watchGetAllWarehouses() {
  yield takeEvery(
    warehouseActionTypes.getAllAsync,
    getAllWarehouseWorker,
  );
}

export default function* rootWarehouseSaga() {
  yield all([
    watchGetAllWarehouses(),
  ]);
}
