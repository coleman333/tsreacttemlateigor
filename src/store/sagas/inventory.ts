import axios from 'axios';
axios.defaults.baseURL = process.env.BASE_URL;

import {
  put,
  takeEvery,
  all,
} from 'redux-saga/effects';

import { inventoryActionTypes } from '../types/actionTypes';
import inventory from '../../services/inventory';
import { TableItems, PageOptions } from '../../store/types/table';

type Props = {
  type: string,
  filters: TableItems,
  pageOptions: PageOptions,
};

function* getInventorySortedWorker(props: Props) {
  const { filters, pageOptions } = props;

  yield put({
    type: inventoryActionTypes.getSorted,
    payload: { count: null, status: 'fetching' },
  });

  const data = yield inventory.getSorted(filters, pageOptions);

  (!data.status || data.status !== "error")
    ?  yield put({
      type: inventoryActionTypes.getSorted,
      payload: {
        data: data.data,
        status: 'fetched',
      },
    })
    : yield put({
      type: inventoryActionTypes.getSorted,
      payload: { data: null, status: 'error' },
    });
}

function* watchGetInventorytSorted() {
  yield takeEvery(
    inventoryActionTypes.getSortedAsync,
    getInventorySortedWorker,
  );
}

export default function* rootInventorySaga() {
  yield all([
    watchGetInventorytSorted(),
  ]);
}
