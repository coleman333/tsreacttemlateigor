import axios from 'axios';
axios.defaults.baseURL = process.env.BASE_URL;

import {
  call,
  takeEvery,
  all,
  put,
} from 'redux-saga/effects';

import { statistic } from '../types/actionTypes';
// tslint:disable-next-line: import-name
import statisticServices from '../../services/statistic';

function* getEquipmentCountTotal() {
  yield put({
    type: statistic.getEquipmentCount,
    payload: { status: 'fetching' },
  });

  const data = yield call(statisticServices.getEquipmentCount);

  (!data.status || data.status !== "error")
    ? yield put({
      type: statistic.getEquipmentCount,
      payload: { data: data.data.equipmentCount, status: 'fetched' },
    })
    : yield put({
      type: statistic.getEquipmentCount,
      payload: { status: data.status },
    });
}

function* getInventoryCountTotal() {
  yield put({
    type: statistic.getInventoryCount,
    payload: { status: 'fetching' },
  });

  const data = yield call(statisticServices.getInventoryCount);

  (!data.status || data.status !== "error")
    ? yield put({
      type: statistic.getInventoryCount,
      payload: { data: data.data.totalInventory, status: 'fetched' },
    })
    : yield put({
      type: statistic.getInventoryCount,
      payload: { status: data.status },
    });
}

function* getInventoryCountMarked() {
  yield put({
    type: statistic.getMarkedInventory,
    payload: { status: 'fetching' },
  });

  const data = yield call(statisticServices.getInventoryCountMarked);

  (!data.status || data.status !== "error")
    ? yield put({
      type: statistic.getMarkedInventory,
      payload: { data: data.data.inventoryCountMarked, status: 'fetched' },
    })
    : yield put({
      type: statistic.getMarkedInventory,
      payload: { status: data.status },
    });
}

function* getInventoryCountUnmarked() {
  yield put({
    type: statistic.getUnMarkedInventory,
    payload: { status: 'fetching' },
  });

  const data = yield call(statisticServices.getInventoryCountUnmarked);

  (!data.status || data.status !== "error")
    ? yield put({
      type: statistic.getUnMarkedInventory,
      payload: { data: data.data.inventoryCountUnMarked, status: 'fetched' },
    })
    : yield put({
      type: statistic.getUnMarkedInventory,
      payload: { status: data.status },
    });
}

function* getInventoryCountMounted() {
  yield put({
    type: statistic.getMountedInventory,
    payload: { status: 'fetching' },
  });

  const data = yield call(statisticServices.getInventoryCountMounted);

  (!data.status || data.status !== "error")
    ? yield put({
      type: statistic.getMountedInventory,
      payload: { data: data.data.inventoryCountMounted, status: 'fetched' },
    })
    : yield put({
      type: statistic.getMountedInventory,
      payload: { status: data.status },
    });
}

function* getInventoryCountUnmounted() {
  yield put({
    type: statistic.getUnmountedInventory,
    payload: { status: 'fetching' },
  });

  const data = yield call(statisticServices.getInventoryCountUnmounted);

  (!data.status || data.status !== "error")
    ? yield put({
      type: statistic.getUnmountedInventory,
      payload: { data: data.data.inventoryCountUnMounted, status: 'fetched' },
    })
    : yield put({
      type: statistic.getUnmountedInventory,
      payload: { status: data.status },
    });
}

function* watchGetInventoryCountAll() {
  yield takeEvery(statistic.getAllInventoryAsync, getEquipmentCountTotal);
  yield takeEvery(statistic.getAllInventoryAsync, getInventoryCountTotal);
  yield takeEvery(statistic.getAllInventoryAsync, getInventoryCountMarked);
  yield takeEvery(statistic.getAllInventoryAsync, getInventoryCountUnmarked);
  yield takeEvery(statistic.getAllInventoryAsync, getInventoryCountMounted);
  yield takeEvery(statistic.getAllInventoryAsync, getInventoryCountUnmounted);
}

export default function* rootStatisticSaga() {
  yield all([
    watchGetInventoryCountAll(),
  ]);
}
