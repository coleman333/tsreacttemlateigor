import { Action } from '../types/actions';
import { inventoryActionTypes, tableActionTypes, filterActionTypes } from '../types/actionTypes';
import { StateUnit } from '../types/stateUnit';

const initialState: InitialState = {
  sorted:<StateUnit> {
    data: null,
    status: 'init',
  },
  activeFields: { },
};

type InitialState = {
  sorted: StateUnit,
  activeFields: { },
};

export const inventoryReducer = (state:InitialState = initialState, action: Action) => {
  switch (action.type) {

    case filterActionTypes.inventory: {
      return {
        ...state,
        activeFields: {
          ...state.activeFields,
          [action.payload.field]: {
            name: state.activeFields[action.payload.field].name,
            status: action.payload.status || state.activeFields[action.payload.field].status,
            filter: {
              value: action.payload.filter,
              filterType: state.activeFields[action.payload.field].filter.filterType,
            },
          },
        },
      };
    }

    case tableActionTypes.setAllInventory: {
      const _activeFields = {};

      if (action.payload.data !== null) {
        action.payload.data.forEach((field: any) => {
          _activeFields[field.key] = {
            name: field.name,
            status: true,
            filter: {
              value: '',
              filterType: field.filterType
              ? field.filterType
              : state.activeFields[field.key].filter.filterType,
            },
          };
        });
      }

      return {
        ...state,
        activeFields: _activeFields,
      };
    }

    case tableActionTypes.setInventory: {
      return {
        ...state,
        activeFields: {
          ...state.activeFields,
          [action.payload.key]: {
            name: action.payload.name
            ? action.payload.name
            : state.activeFields[action.payload.key].name,
            status: action.payload.value,
            filter: {
              value: '',
              filterType: action.payload.filterType ?
                action.payload.filterType :
                state.activeFields[action.payload.key].filter.filterType,
            },
          },
        },
      };
    }

    case inventoryActionTypes.getSorted: {
      return {
        ...state,
        sorted: {
          data: action.payload.data || state.sorted.data,
          status: action.payload.status,
        },
      };
    }

    default:
      return state;
  }
};
