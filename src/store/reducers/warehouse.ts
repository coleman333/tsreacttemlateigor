import { Action } from '../types/actions';
import { warehouseActionTypes } from '../types/actionTypes';

const initialState: State = {
  data: null,
  status: 'init',
};

type State = {
  data: Object | null,
  status: string,
};

export const warehouseReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    case warehouseActionTypes.getAll  : {
      return {
        data: action.payload.data || state.data,
        status: action.payload.status,
      };
    }

    default:
      return state;
  }
};
