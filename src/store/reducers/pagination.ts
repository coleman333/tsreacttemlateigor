
import { pagesActionTypes } from '../types/actionTypes';

type PagesAction = {
  type: string,
  payload: {
    itemsPerPage?: number,
    activePage?: number,
    count?: number | null,
    status: 'init' | 'fetching' | 'fetched' | 'error',
  },
};

const initialState = {
  itemsPerPage: 10,
  activePage: 0,
  distinctEquipment: {
    count: null,
    status: 'init',
  },
  distinctInventory: {
    count: null,
    status: 'init',
  },
};

export const paginationReducer = (state = initialState, action: PagesAction) => {
  switch (action.type) {

    case pagesActionTypes.getEquipmentDistinct: {
      return {
        ...state,
        distinctEquipment: {
          count: action.payload.count || state.distinctEquipment.count,
          status: action.payload.status,
        },
      };
    }

    case pagesActionTypes.getInventoryDistinct: {
      return {
        ...state,
        distinctInventory: {
          count: action.payload.count || state.distinctInventory.count,
          status: action.payload.status,
        },
      };
    }

    case pagesActionTypes.setPageCount: {
      return {
        ...state,
        itemsPerPage: action.payload.itemsPerPage,
      };
    }

    case pagesActionTypes.setActivePage: {
      return {
        ...state,
        activePage: action.payload.activePage,
      };
    }

    default: {
      return state;
    }
  }
};
