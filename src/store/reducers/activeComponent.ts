import { componentActionTypes } from '../types/actionTypes';

type InitialState = '';

type ComponentAction = {
  type: string,
  payload: string,
};

const initialState: InitialState = '';

export const activeComponentReducer =
  (state:InitialState = initialState, action: ComponentAction) => {
    switch (action.type) {

      case componentActionTypes.set: {
        return action.payload;
      }

      default:
        return state;
    }
  };
