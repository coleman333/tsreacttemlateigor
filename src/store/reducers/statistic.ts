import { Action } from '../types/actions';
import { statistic } from '../types/actionTypes';

interface StateUnit {
  count: number | null;
  status: 'init' | 'fetching' | 'fetched' | 'error';
}

interface InitialState {
  equipment: {
    totalEquipment: StateUnit,
  };
  inventory: {
    totalInventory: StateUnit,
    mountedInventory: StateUnit,
    unmountedInventory: StateUnit,
    markedInventory: StateUnit,
    unmarkedInventory: StateUnit,
  };
}

const initialState:InitialState = {
  equipment: {
    totalEquipment: {
      count: null,
      status: 'init',
    },
  },
  inventory: {
    totalInventory: {
      count: null,
      status: 'init',
    },
    mountedInventory: {
      count: null,
      status: 'init',
    },
    unmountedInventory: {
      count: null,
      status: 'init',
    },
    markedInventory: {
      count: null,
      status: 'init',
    },
    unmarkedInventory: {
      count: null,
      status: 'init',
    },
  },
};

export const statisticReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case statistic.resetStatistic: {
      return initialState;
    }

    case statistic.getEquipmentCount: {
      return {
        ...state,
        equipment: {
          ...state.equipment,
          totalEquipment: {
            count: action.payload.data || state.equipment.totalEquipment.count,
            status: action.payload.status,
          },
        },
      };
    }

    case statistic.getInventoryCount: {
      return {
        ...state,
        inventory: {...state.inventory,
          totalInventory: {
            count: action.payload.data || state.inventory.totalInventory.count,
            status: action.payload.status,
          },
        },
      };
    }

    case statistic.getMountedInventory: {
      return {
        ...state,
        inventory: {...state.inventory,
          mountedInventory: {
            count: action.payload.data || state.inventory.mountedInventory.count,
            status: action.payload.status,
          },
        },
      };
    }

    case statistic.getUnmountedInventory: {
      return {
        ...state,
        inventory: {...state.inventory,
          unmountedInventory: {
            count: action.payload.data || state.inventory.unmountedInventory.count,
            status: action.payload.status,
          },
        },
      };
    }

    case statistic.getMarkedInventory: {
      return {
        ...state,
        inventory: {...state.inventory,
          markedInventory: {
            count: action.payload.data || state.inventory.markedInventory.count,
            status: action.payload.status,
          },
        },
      };
    }

    case statistic.getUnMarkedInventory: {
      return {
        ...state,
        inventory: {...state.inventory,
          unmarkedInventory: {
            count: action.payload.data || state.inventory.unmarkedInventory.count,
            status: action.payload.status,
          },
        },
      };
    }

    default:
      return state;
  }
};
