import { combineReducers } from 'redux';

import { equipmentReducer } from './equipment';
import { inventoryReducer } from './inventory';
import { statisticReducer } from './statistic';
import { activeComponentReducer } from './activeComponent';
import { paginationReducer } from './pagination';
import { warehouseReducer } from './warehouse';

export default combineReducers({
  equipment: equipmentReducer,
  inventory: inventoryReducer,
  statistic: statisticReducer,
  activeComponent: activeComponentReducer,
  pagination: paginationReducer,
  warehouses: warehouseReducer,
});
