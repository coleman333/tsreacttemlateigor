export const statistic = {
  getEquipmentCount: 'GET_COUNT_ALL_EQUIPMENT',
  getEquipmentCountAsync: 'GET_COUNT_ALL_EQUIPMENT_ASYNC',
  getInventoryCount: 'GET_COUNT_ALL_INVENTORY',
  getAllInventoryAsync: 'GET_COUNT_ALL_INVENTORY_ASYNC',
  getMountedInventory: 'GET_MOUNTED_INVENTORY',
  getUnmountedInventory: 'GET_UNMOUNTED_INVENTORY',
  getMarkedInventory: 'GET_MARKED_INVENTORY',
  getUnMarkedInventory: 'GET_UNMARKED_INVENTORY',
  resetStatistic: 'RESET_STATISTIC',
};

export const equipmentActionTypes = {
  getSortedAsync: 'GET_EQUIPMENT_SORTED_ASYNC',
  getSorted: 'GET_EQUIPMENT_SORTED',
};

export const inventoryActionTypes = {
  getSortedAsync: 'GET_INVENTORY_SORTED_ASYNC',
  getSorted: 'GET_INVENTORY_SORTED',
};

export const componentActionTypes = {
  set: 'SET_ACTIVE_COMPONENT',
};

export const tableActionTypes = {
  setEquipment: 'SET_EQUIPMENT_TABLE_FIELD',
  setAllEquipment: 'SET_ALL_EQUIPMENT_TABLE_FIELDS',
  setInventory: 'SET_INVENTORY_TABLE_FIELD',
  setAllInventory: 'SET_ALL_INVENTORY_TABLE_FIELDS',
};

export const filterActionTypes = {
  equipment: 'SET_EQUIPMENT_TABLE_FILTER',
  inventory: 'SET_INVENTORY_TABLE_FILTER',
};

export const pagesActionTypes = {
  setPageCount: 'SET_PAGE_COUNT',
  setActivePage: 'SET_ACTIVE_PAGE',
  getEquipmentDistinct: 'GET_EQUIPMENT_DISTINCT_COUNT',
  getEquipmentDistinctAsync: 'GET_EQUIPMENT_DISTINCT_COUNT_ASYNC',
  getInventoryDistinct: 'GET_INVENTORY_DISTINCT_COUNT',
  getInventoryDistinctAsync: 'GET_INVENTORY_DISTINCT_COUNT_ASYNC',
};

export const warehouseActionTypes = {
  getAll: 'GET_ALL_WAREHOUSE',
  getAllAsync: 'GET_ALL_WAREHOUSE_ASYNC',
};
