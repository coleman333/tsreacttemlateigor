export interface StateUnit {
  data: Object | null;
  status: 'init' | 'fetching' | 'fetched' | 'error';
}
