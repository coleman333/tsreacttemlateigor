export type Action = {
  type: any,
  payload: {
    data: any[] | null,
    status: 'init' | 'fetching' | 'fetched'
    field: string,
    value?: boolean,
    filter?: string,
    filterType?: string,
    key: string,
    name?: string,
  },
};
