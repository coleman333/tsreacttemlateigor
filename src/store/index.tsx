import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
// tslint:disable-next-line: import-name
import createSagaMiddleware from 'redux-saga';

import reducers from './reducers';

import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  reducers,
  composeWithDevTools(
    applyMiddleware(sagaMiddleware),
  ),
);

sagaMiddleware.run(sagas);
