import axios from 'axios';
import { TableItems, PageOptions } from '../store/types/table';

axios.defaults.baseURL = process.env.BASE_URL;

const initParams: PageOptions = {
  limit: 10,
  offset: 0,
};

export default {
  getEquipmentCountDistinct: async () => {
    try {
      return await axios.get('equipment/count-all');
    } catch (error) {
      return { status: 'error' };
    }
  },
  getSorted: async (params?: TableItems, pageOptions?: PageOptions) => {
    const _params = Object.assign(initParams, params, pageOptions);

    try {
      return await axios.post('equipment/equipment', _params);
    } catch (error) {
      return { status: 'error' };
    }
  },
};
