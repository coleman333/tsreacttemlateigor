import axios from 'axios';

axios.defaults.baseURL = process.env.BASE_URL;

export default {
  getAll: async () => {
    try {
      return await axios.get('warehouse/warehouses');
    } catch (error) {
      return { status: 'error' };
    }
  },
};
