import axios from 'axios';
import { TableItems } from '../store/types/table';

axios.defaults.baseURL = process.env.BASE_URL;

type PageOptions = {
  limit: number,
  offset: number,
};

const initParams: PageOptions = {
  limit: 10,
  offset: 0,
};

export default {
  getSorted: async (params?: TableItems, pageOptions?: PageOptions) => {
    const _params = Object.assign(initParams, params, pageOptions);

    try {
      return await axios.post('inventory/inventory', _params);
    } catch (error) {
      return { status: 'error' };
    }
  },
  getDistinctCount: async () => {
    try {
      return await axios.get('inventory/count-all');
    } catch (error) {
      return { status: 'error' };
    }
  },
};
