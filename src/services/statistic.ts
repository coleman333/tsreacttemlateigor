import axios from 'axios';

axios.defaults.baseURL = process.env.BASE_URL;

export default {
  getEquipmentCount: async () => {
    try {
      return await axios.get('equipment/count-all');
    } catch (error) {
      return { status: 'error' };
    }
  },
  getInventoryCount: async () => {
    try {
      return await axios.get('inventory/count-all');
    } catch (error) {
      return { status: 'error' };
    }
  },
  getInventoryCountMounted: async () => {
    try {
      return await axios.get('inventory/count-all-mounted');
    } catch (error) {
      return { status: 'error' };
    }
  },
  getInventoryCountUnmounted: async () => {
    try {
      return await axios.get('inventory/count-all-unmounted');
    } catch (error) {
      return { status: 'error' };
    }
  },
  getInventoryCountMarked: async () => {
    try {
      return await axios.get('inventory/count-all-marked');
    } catch (error) {
      return { status: 'error' };
    }
  },
  getInventoryCountUnmarked: async () => {
    try {
      return await axios.get('inventory/count-all-unmarked');
    } catch (error) {
      return { status: 'error' };
    }
  },
};
