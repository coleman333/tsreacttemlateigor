import * as React from 'react';
import { Route, Redirect,  Switch } from 'react-router-dom';

import './App.css';

import { LeftMenu } from './components/LeftMenu';
import Statistic from './containers/Statistic/Statistic';
import Inventory from './containers/Inventory/Inventory';
import Equipment from './containers/Equipment/Equipment';
import Storehouse from './containers/Storehouse/Storehouse';

export const App = () => {
  return (
    <div className='app-wrapper'>
      <LeftMenu />
      <Switch>
{/* tslint:disable-next-line: jsx-boolean-value */}
        <Route exact path="/" component={Statistic} />
        <Route path="/inventory" component={Inventory} />
        <Route path="/equipment" component={Equipment} />
        <Route path="/storehouse" component={Storehouse} />
{/* tslint:disable-next-line: jsx-no-lambda */}
        <Route render={() => <Redirect to='/' />} />
      </Switch>
    </div>
  );
};
