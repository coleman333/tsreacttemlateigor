import * as React from 'react';
import Equipment from './Equipment';
import * as enzyme from 'enzyme';

test('Equipment renders', () => {
  const component = enzyme.shallow(<Equipment />);

  expect(component).toMatchSnapshot();
});
