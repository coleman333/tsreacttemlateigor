import * as React from 'react';
const { useCallback, useState } = React;
import { useMappedState, useDispatch } from 'redux-react-hook';

import { Table } from '../../components/Table/Table';
import { Spinner } from '../../components/Spinner/Spinner';
import { Wrapped } from '../../HOC/ContainerWrapper';

import { componentActionTypes, equipmentActionTypes } from '../../store/types/actionTypes';
import helpers from '../../helpers';

type PageOptionsState = {
  limit: null | string | number,
  offset: null | string | number,
};

const Equipment = () => {
  const [prevActiveFields, setPrevActiveFields] = useState<{}>({});
  const [prevPageOptions, setPrevPageOptions] = useState<PageOptionsState>(
    { limit: null, offset: null },
    );

  const dispatch = useDispatch();

  const mapState = useCallback(
    state => ({
      data: state.equipment.sorted.data,
      activeFields: state.equipment.activeFields,
      status: state.equipment.sorted.status,
      pageOptions: {
        limit: state.pagination.itemsPerPage,
        offset: state.pagination.itemsPerPage * state.pagination.activePage,
      },
    }),
    [],
  );

  const {
    data,
    activeFields,
    status,
    pageOptions,
  } = useMappedState(mapState);

  useCallback(
    () => {
      dispatch({ type: componentActionTypes.set, payload: 'equipment' });
    },
    [],
  );

  if (status !== 'fetching') {
    const filters = {};

    for (const key in activeFields) {
      filters[key] = activeFields[key]['filter']['value'];
    }

    if (
      (Object.keys(activeFields).length !== 0)
      && !helpers.deepEquals(prevActiveFields, activeFields)
    ) {
      setPrevActiveFields(helpers.deepCopy(activeFields));

      dispatch({
        filters,
        pageOptions,
        type: equipmentActionTypes.getSortedAsync,
      });
    }

    if (pageOptions && !helpers.deepEquals(prevPageOptions, pageOptions)) {
      setPrevPageOptions(pageOptions);

      dispatch({
        filters,
        pageOptions,
        type: equipmentActionTypes.getSortedAsync,
      });
    }
  }

  return (
    <React.Fragment>
      {data ? <Table /> : <Spinner />}
    </React.Fragment>
  );
};

export default Wrapped(Equipment);
