import * as React from 'react';
const { useCallback, useState }  = React;
import { useMappedState, useDispatch } from 'redux-react-hook';

import { Table } from '../../components/Table/Table';
import { Spinner } from '../../components/Spinner/Spinner';
import { Wrapped } from '../../HOC/ContainerWrapper';

import { componentActionTypes, inventoryActionTypes } from '../../store/types/actionTypes';
import helpers from '../../helpers';

type State = {
  limit: null | string | number,
  offset: null | string | number,
};

const Inventory = () => {
  const [init, setInit] = useState(false);
  const [prevActiveFields, setPrevActiveFields] = useState<{} | null>(null);
  const [prevPageOptions, setPrevPageOptions] = useState<State>({
    limit: null,
    offset: null,
  });

  const dispatch = useDispatch();

  const mapState = useCallback(
    state => ({
      data: state.inventory.sorted.data,
      status: state.inventory.sorted.status,
      activeFields: state.inventory.activeFields,
      pageOptions: {
        limit: state.pagination.itemsPerPage,
        offset: state.pagination.itemsPerPage * state.pagination.activePage,
      },
    }),
    [],
    );

  const {
    status,
    data,
    activeFields,
    pageOptions,
  } = useMappedState(mapState);

  // Set current component active
  if (!init) {
    setInit(true);
    dispatch({ type: componentActionTypes.set, payload: 'inventory' });
  }

  if (status !== 'fetching') {
    const filters = {};

    for (const key in activeFields) {
      filters[key] = activeFields[key]['filter']['value'];
    }

    if (
      (Object.keys(activeFields).length !== 0)
      && !helpers.deepEquals(prevActiveFields, activeFields)
    ) {
      setPrevActiveFields(helpers.deepCopy(activeFields));

      dispatch({
        filters,
        pageOptions,
        type: inventoryActionTypes.getSortedAsync,
      });

    } else if (pageOptions && !helpers.deepEquals(prevPageOptions, pageOptions)
    ) {
      setPrevPageOptions(helpers.deepCopy(pageOptions));

      dispatch({
        filters,
        pageOptions,
        type: inventoryActionTypes.getSortedAsync,
      });
    }
  }

  return (
    <React.Fragment>
      {data ? <Table /> :  <Spinner />}
    </React.Fragment>
  );
};

export default Wrapped(Inventory);
