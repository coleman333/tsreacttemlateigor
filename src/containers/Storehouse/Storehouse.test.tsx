import * as React from 'react';
import Storehouse from './Storehouse';
import * as enzyme from 'enzyme';

test('Pumts renders', () => {
  const component = enzyme.shallow(<Storehouse />);

  expect(component).toMatchSnapshot();
});
