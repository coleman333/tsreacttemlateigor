import * as React from 'react';
import { Wrapped } from '../../HOC/ContainerWrapper';
import { useDispatch, useMappedState } from 'redux-react-hook';
import { componentActionTypes, warehouseActionTypes } from '../../store/types/actionTypes';
import { CustomSelect } from '../../components/CustomSelect/CustomSelect';
import { deepEquals } from '../../helpers/deepEquals';
import { deepCopy } from '../../helpers/deepCopy';

type Item = {
  id: string,
  warehouse_name: string,
};

const _customers = [
  { id: '1', warehouse_name: 'first customer' },
  { id: '2', warehouse_name: 'second customer' },
  { id: '3', warehouse_name: 'third customer' },
  { id: '4', warehouse_name: 'forth customer' },
  { id: '5', warehouse_name: 'fifth customer' },
];

const _storage = [
  { id: '1', warehouse_name: 'first storage' },
  { id: '2', warehouse_name: 'second storage' },
  { id: '3', warehouse_name: 'third storage' },
  { id: '4', warehouse_name: 'forth storage' },
  { id: '5', warehouse_name: 'fifth storage' },
];

const _production = [
  { id: '1', warehouse_name: 'first production' },
  { id: '2', warehouse_name: 'second production' },
  { id: '3', warehouse_name: 'third production' },
  { id: '4', warehouse_name: 'forth production' },
  { id: '5', warehouse_name: 'fifth production' },
];

type StateType = Item[] | null;

const Storehouse = () => {
  const [prevMain, setPrevMain] = React.useState<StateType>(null);
  const [customers, setCustomers] = React.useState<StateType>(null);
  const [storage, setStorage] = React.useState<StateType>(null);
  const [production, setProduction] = React.useState<StateType>(null);

  const [activeMain, setActiveMain] = React.useState('');
  const [activeCustomer, setActiveCustomer] = React.useState('');
  const [activeProduction, setActiveProduction] = React.useState('');
  const [activeStorage, setActiveStorage] = React.useState('');

  const mapState = React.useCallback(
    state => ({
      main: state.warehouses,
    }),
    [],
  );

  const { main } = useMappedState(mapState);

  const dispatch = useDispatch();

  // Get warehouses on init
  React.useEffect(
    () => {
      dispatch({
        type: warehouseActionTypes.getAllAsync,
      });
    },
    [],
  );

  console.log('render');

  // Compare warehouses
  if (main.status === 'fetched') {
    if (!deepEquals(main, deepCopy(prevMain))) {
      setPrevMain(deepCopy(main));

      dispatch({
        type: warehouseActionTypes.getAllAsync,
      });
    }
  }

  if (!deepEquals(customers, _customers)) {
    setCustomers(_customers);
  }

  if (!deepEquals(storage, _storage)) {
    setStorage(_storage);
  }

  if (!deepEquals(production, _production)) {
    setProduction(_production);
  }

  dispatch({ type: componentActionTypes.set, payload: 'pumts' });

  const handleChooseMain = (val: string) => {
    setActiveMain(val);
  };

  const handleChooseCustomer = (val: string) => {
    setActiveCustomer(val);
  };

  const handleChooseStorage = (val: string) => {
    setActiveStorage(val);
  };

  const handleChooseProduction = (val: string) => {
    setActiveProduction(val);
  };

  if (false) {
    console.log('activeMain :', activeMain);
    console.log('activeCustome :', activeCustomer);
    console.log('activeStorage :', activeStorage);
    console.log('activeProduction :', activeProduction);
  }

  return (
    <React.Fragment>
      <h2>ПУМТС</h2>
      {
        main.data && <CustomSelect
          title='Склади ПУМТС'
          list={main.data}
          choose={handleChooseMain}
        />
      }
      <div>
        <CustomSelect
          title='Підприємства'
          list={customers}
          choose={handleChooseCustomer}
        />
        {
          (activeCustomer !== '') && (
            <CustomSelect
              title='Склади підприємства'
              list={storage}
              choose={handleChooseStorage}
            />
          )
        }
        {
          (activeCustomer !== '') && (
          <CustomSelect
            title='Промисловiсть підприємства'
            list={production}
            choose={handleChooseProduction}
          />
          )
        }
      </div>
    </React.Fragment>
  );
};

export default Wrapped(Storehouse);
