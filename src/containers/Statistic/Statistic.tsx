import * as React from 'react';
const { useCallback } = React;

import { useDispatch, useMappedState } from 'redux-react-hook';
import { componentActionTypes, statistic } from '../../store/types/actionTypes';

import { StatisticGrafics } from '../../components/StatisticGrafics/StatisticGrafics';
import { StatisticTree } from '../../components/StatisticTree/StatisticTree';
import { Wrapped } from '../../HOC/ContainerWrapper';

const Statistic = () => {
  const dispatch = useDispatch();

  const mapState = useCallback(
    state => ({
      activeComponent: state.activeComponent,
    }),
    [],
  );

  const { activeComponent } = useMappedState(mapState);

  // Set current component active
  if (activeComponent !== 'statistic') {
    dispatch({ type: componentActionTypes.set, payload: 'statistic' });

    dispatch({ type: statistic.getAllInventoryAsync });
  }

  return (
    <React.Fragment>
      <h2>Статистика</h2>
      <div>
        <StatisticTree />
        <StatisticGrafics />
      </div>
    </React.Fragment>
  );
};

export default Wrapped(Statistic);
