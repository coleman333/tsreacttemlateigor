export const deepEquals = (x:any, y:any) => {
  if ((x === undefined) && (y === undefined)) {
    return true;
  }

  if (x === y) {
    return true;
  }

  if ((x === null) && (y === null)) {
    return true;
  }

  if (((typeof x === "object") && (x !== null)) && ((typeof y === "object") && (y !== null))) {
    if (Object.keys(x).length !== Object.keys(y).length) {
      return false;
    }

    for (const prop in x) {
      if (y.hasOwnProperty(prop)) {
        if (!deepEquals(x[prop], y[prop])) {
          return false;
        }
      } else {
        return false;
      }
    } return true;
  }

  return false;
};
