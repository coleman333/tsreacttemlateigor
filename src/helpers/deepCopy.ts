type El = any;

export const deepCopy = (el: El):El => {
  if (el === null) {
    return null;
  }
  if (typeof el !== 'object') {
    const newEl = el;
    return newEl;
  }

  if (el instanceof Object) {
    if (el instanceof Array) {
      const newEl = el.map((item) => {
        return deepCopy(item);
      });

      return newEl;

    }
    if (el instanceof Function) {
      return el;
    }

    const newEl = {};

    Object.keys(el).forEach((key) => {
      newEl[key] = deepCopy(el[key]);
    });

    return newEl;
  }
};
