import { deepCopy } from './deepCopy';
import { deepEquals } from './deepEquals';
import { capitalize } from './capitalize';

export default {
  deepCopy,
  deepEquals,
  capitalize,
};
