export const capitalize = (str: string): string => {
  return str.toLowerCase().replace(/\b./g, a => a.toUpperCase());
};
