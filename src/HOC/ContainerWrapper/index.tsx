import * as React from 'react';
import './ContainerWrapper.css';

export const Wrapped = (WrappedComponent:any) => {
  return function HOC() {
    return (
      <section className='section-wrapper'>
        <WrappedComponent />
      </section>
    );
  };
};
