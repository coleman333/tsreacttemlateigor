import * as React from 'react';
import { Logo } from './Logo';
import * as enzyme from 'enzyme';

test('Logo renders', () => {
  const component = enzyme.shallow(<Logo />);

  expect(component).toMatchSnapshot();
});
