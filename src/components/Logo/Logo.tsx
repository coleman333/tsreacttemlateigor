import * as React from 'react';
import { Link } from 'react-router-dom';
import './Logo.css';

export const Logo = () => {
  const handleDrag = (e: React.DragEvent<HTMLAnchorElement>)  => {
    e.preventDefault();
  };

  return (
    <div className='logo-wrapper'>
      <Link onDragStart={handleDrag} to='/'>
        <span>
          DTEK
        </span>
      </Link>
    </div>
  );
};
