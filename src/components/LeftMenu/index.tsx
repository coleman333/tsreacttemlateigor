import * as React from 'react';
const { useCallback } = React;
import { useMappedState } from 'redux-react-hook';

import { Item } from './Components/Item';
import { Logo } from '../Logo/Logo';
import { SelectColumns } from '../SelectColumns/SelectColumns';

import './LeftMenu.css';

const list = [
  { path: '/statistic', name: 'Статистика' },
  { path: '/inventory', name: 'ТМЦ' },
  { path: '/equipment', name: 'Обладнання' },
  { path: '/storehouse', name: 'Склади' },
];

export const LeftMenu = () => {
  const mapState = useCallback(
    state => ({
      activeComponent: state.activeComponent,
    }),
    [],
  );

  const { activeComponent } = useMappedState(mapState);

  const renderList = list.map((item, idx) => {
    return (
      <Item
        key={idx}
        path={item.path}
        name={item.name}
        isActive={item.path.includes(activeComponent)}
      />
    );
  });

  let show;
  if (activeComponent) {
    if (
      (activeComponent === 'inventory')
      || (activeComponent === 'equipment')
    ) {
      show = true;
    }
  } else {
    show = false;
  }

  return (
    <section className='left-menu-wrapper'>
    <nav>
      <Logo />
        <ul className='left-menu-list'>
          {renderList}
        </ul>
    </nav>

    {show && <SelectColumns />}
    </section>
  );
};
