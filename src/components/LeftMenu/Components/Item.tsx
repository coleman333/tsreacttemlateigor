import * as React from 'react';
import { Link }from 'react-router-dom';

import '../LeftMenu.css';

export type ItemProps = {
  path: string,
  name: string,
  isActive: boolean,
};

export const Item = (props: ItemProps) => {
  const { path, name, isActive } = props;

  let _path = path;

  if (path === '/statistic') {
    _path = '/';
  }

  const className = isActive ? 'left-menu-list-item active' : 'left-menu-list-item';

  const handleDrag = (e: React.DragEvent<HTMLAnchorElement>) => {
    e.preventDefault();
  };

  return (
    <li className={className}>
      <Link onDragStart={handleDrag} to={_path}>{name}</Link>
    </li>
  );
};
