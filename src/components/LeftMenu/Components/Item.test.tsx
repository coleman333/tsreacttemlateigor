import * as React from 'react';
import { Item, ItemProps } from './Item';
import * as enzyme from 'enzyme';

const _mockProps:ItemProps = {
  path: 'path',
  name: 'name',
  isActive: true,
};

test('Item renders', () => {
  const component = enzyme.shallow(<Item {..._mockProps}/>);

  expect(component).toMatchSnapshot();
});
