import * as React from 'react';

import './StatisticGrafics.css';

import Chart from '../Chart/Chart';

export const StatisticGrafics = () => {
  const charts = [
    {
      name: 'mounted',
      title: 'Змонтовано',
    },
    {
      name: 'marked',
      title: 'Промарковано',
    },
  ];

  return (
    <div className='statistic-grafics-wrapper'>
      {
        charts.map((chart, idx) => {
          return (
            <Chart key={idx} item={chart}/>
          );
        })
      }
    </div>
  );
};
