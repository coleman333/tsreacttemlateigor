import * as React from 'react';
import './index.css';
import { svg } from '../../../../assets/';

const { CloseIconSvg } = svg;

export type Item = {
  id: string,
  name: string,
};

type CustomLiProps = {
  item: any,
  chooseItem: (val: Item) => void,
  deleteItem: (val: Item) => void,
  openModal: (val: Item) => void,
  active: boolean,
};

export const CustomLi = (props: CustomLiProps) => {
  const { item, chooseItem, active, deleteItem, openModal } = props;

  const handleChoose = () => {
    chooseItem(item);
  };

  const handleDelete = () => {
    deleteItem(item);
  };

  const handleShowModal = () => {
    openModal(item);
  };

  const className = active ? 'custom-select-item active' : 'custom-select-item';

  return (
    <React.Fragment>
      <li
        className={className}
        onClick={handleChoose}
      >
          <span >{item.warehouse_name} </span>
          <span onClick={handleShowModal}>change </span>
          <span onClick={handleDelete}>
            <CloseIconSvg width='15' height='15' />
          </span>
      </li>
    </React.Fragment>
  );
};
