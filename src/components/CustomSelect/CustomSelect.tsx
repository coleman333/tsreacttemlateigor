import * as React from 'react';
import './CustomSelect.css';

import { CustomLi, Item } from './Components/CustomLi/';
import { Modal } from '../Modal/Modal';

type CustomSelectProps = {
  list: any[] | null,
  title: string,
  choose: (val: string) => void,
};

export const CustomSelect = (props: CustomSelectProps):React.ReactElement => {
  const [selected, setSelected] = React.useState('');
  const [filter, setFilter] = React.useState('');
  const [isOpenedList, setIsOpenedList] = React.useState(false);
  const [isOpenedModal, setIsOpenedModal] = React.useState(false);

  const { list, title, choose } = props;

  const handleChoose = (item: Item) => {
    if (item.id === selected) return;

    setSelected(item.id);

    choose(item.id);
  };

  const handleDelete = (item: Item) => {
    // console.log(`deleting: ${item.id}`);
  };

  const handleOpenModal = (item: Item) => {
    // console.log(`opening modal for ${item.id}`);
    setIsOpenedModal(true);
  };

  const handleCloseModal = () => {
    setIsOpenedModal(false);
  };

  const toggleOpen = () => {
    setIsOpenedList(!isOpenedList);

    if (isOpenedList) {
      handleCloseModal();
    }
  };

  const handleFocus = () => {
    setIsOpenedList(true);
  };

  const handleChangeFilter = (e: React.ChangeEvent<HTMLInputElement>) => {
    // console.log(e.target.value);
    setFilter(e.target.value);
  };

  const handleCreate = () => {
    // console.log('Create');
  };

  const listClassName = isOpenedList ? 'custom-select-list' : 'custom-select-list hidden';

  return (
    <div className='custom-select-wrapper'>
      <h3 className='custom-select-header' onClick={toggleOpen}>{title}</h3>

      <div
        className='add-new-item'
        onClick={handleCreate}
      >
        <span>Створити</span>
      </div>
      <div className='custom-select-filter-wrapper'>
        <span>Фiльтр: </span>
        <input
          type='text'
          className='custom-select-filter'
          onChange={handleChangeFilter}
          onFocus={handleFocus}
        />
      </div>

      <div className='custom-select-list'>
        <ul className={listClassName}>
          {list && list.map((item, idx) => {
            if (item.warehouse_name.includes(filter)) {
              return (
                <CustomLi
                  key={idx}
                  active={selected === item.id}
                  item={item}
                  chooseItem={handleChoose}
                  openModal={handleOpenModal}
                  deleteItem={handleDelete}
                />
              );
            }

            return null;
          })}
        </ul>
      </div>
      {isOpenedModal &&
        <Modal
          close={handleCloseModal}
        >
          <ModalContent
            item={list && list.filter(item => item.id === selected)[0]}
            changeItem={() => {}}
            deleteItem={() => {}}
          />
        </Modal>
      }
    </div>
  );
};

interface ModalContentRowProps {
  field: string;
  changeField: () => void;
  deleteField: () => void;
}

export const ModalContentRow = (props: ModalContentRowProps) => {
  const [isChanging, setIsChanging] = React.useState(false);
  const [value, setValue] = React.useState('');
  const { field, changeField, deleteField } = props;

  if (!field) return null;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const handleApply = () => {
    changeField();
    setIsChanging(false);
  };

  const handleCancel = () => {
    setValue('');
    setIsChanging(false);
  };

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    e.nativeEvent.stopImmediatePropagation();
    if (e.keyCode === 27) {
      handleCancel();
    }

    if (e.keyCode === 13) {
      handleApply();
    }
  };

  const handleDelete = () => {
    deleteField();
  };

  const handleSetIsChanging = () => {
    setIsChanging(true);
  };

  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    // console.log('e.target :', e.relatedTarget);
  };

  return (
    <div className='modal-content-row-wrapper'>
      {!isChanging && <span onClick={handleSetIsChanging}>{field}</span>}
      {
        isChanging &&
        <React.Fragment>
          <input
            autoFocus={true}
            type='text'
            value={value}
            onChange={handleChange}
            onBlur={handleBlur}
            onKeyDown={handleKeyDown}
          />
          <span
            className='modal-content-apply-span'
            tabIndex={0}
            onClick={handleApply}
          >
            Apply
          </span>
          <span
            className='modal-content-apply-span'
            tabIndex={0}
            onClick={handleCancel}
          >
            Cancel
          </span>
        </React.Fragment>
      }
      <span onClick={handleDelete}>Delete</span>
    </div>
  );
};

interface ModalContentProps {
  item: Item | null;
  changeItem: () => void;
  deleteItem: () => void;
}

export const ModalContent = (props: ModalContentProps) => {
  const { item, changeItem, deleteItem } = props;
  console.log('item :', item);
  return (
    <React.Fragment>
      {item && Object.keys(item).map((field, idx) => {
        console.log('field :', field);
        console.log('item[field] :', item[field]);
        return (
          <ModalContentRow
            key={idx}
            field={item[field]}
            changeField={changeItem}
            deleteField={deleteItem}
          />
        );
      })}
    </React.Fragment>
  );
};
