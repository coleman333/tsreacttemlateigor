import * as React from 'react';
const { useCallback, useState } = React;
import { useMappedState } from 'redux-react-hook';
import {
  PieChart,
  Pie,
  Cell,
} from 'recharts';

import './Chart.css';
import helpers from '../../helpers';

const COLORS = ['#FFBB28', '#fddb2f', '#FA1B28', '#FF8042'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel: (props: any) => any = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

const Chart = (props: {item: { name: string, title: string } }) => {
  const [data, setData] = useState<any>([]);
  const mapState = useCallback(
    (state) => {
      if (props.item.name === 'marked') {
        return ({
          marked: state.statistic.inventory.markedInventory.count,
          unmarked: state.statistic.inventory.unmarkedInventory.count,
          totalInventory: state.statistic.inventory.totalInventory.count,
        });
      }

      if (props.item.name === 'mounted') {
        return ({
          mounted: state.statistic.inventory.mountedInventory.count,
          unmounted: state.statistic.inventory.unmountedInventory.count,
          totalInventory: state.statistic.inventory.totalInventory.count,
        });
      }
      return ({
        ...state,
      });
    },
    [],
  );

  const {
    marked,
    unmarked,
    mounted,
    unmounted,
    totalInventory,
  } = useMappedState(mapState);

  const { item: { name } } = props;

  if (
    (name === 'mounted')
    && mounted
    && (+mounted !== 0)
    && unmounted
    && (+unmounted !== 0)
    && totalInventory
    && (+totalInventory !== 0)
  ) {
    const _data = [];

    _data.push({ name: 'Змонтовано', value: +mounted });
    _data.push({ name: 'У вiльному запасi', value: +(totalInventory - mounted) });

    if (!helpers.deepEquals(_data, data)) {
      setData(_data);
    }
  }

  if (
    (props.item.name === 'marked')
    && marked
    && (+marked !== 0)
    && unmarked
    && (+unmarked !== 0)
  ) {
    const _data = [];

    _data.push({ name: 'Промарковано', value: +marked });
    _data.push({ name: 'Непромарковано', value: +unmarked });

    if (!helpers.deepEquals(_data, data)) {
      setData(_data);
    }
  }

  if (data.length === 0) {
    return null;
  }

  const cells = data.map((_entry: any, index: any) => {
    return (
      <Cell key={index} fill={COLORS[index % COLORS.length]}/>
    );
  });

  const legend = data.map((entry: any, index: any) => {
    return (
      <div key={index} className='pie-chart-legend-item'>
        <div className='pie-chart-legend-item-span'>{entry.name}</div>
        <div
          className='pie-chart-legend-item-color-box'
          style={{ width: '80px', height: '80px', backgroundColor: COLORS[index % COLORS.length] }}
        >
            <span>{entry.value}</span>
        </div>
      </div>
    );
  });

  return (
    <div className='statistic-grafics-component'>
      <h3 className='statistic-grafics-header'>{props.item.title}</h3>
      <PieChart width={320} height={320} >
        <Pie
          dataKey={'value'}
          data={data}
          cx={160}
          cy={160}
          labelLine={false}
          label={renderCustomizedLabel}
          outerRadius={150}
          fill="#8884d8"
        >
          {cells}
        </Pie>
      </PieChart>
      <div className='pie-chart-legend'>
        {legend}
      </div>
    </div>
  );
};

export default Chart;
