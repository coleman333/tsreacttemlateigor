import * as React from 'react';
import { Spinner } from './Spinner';

import * as enzyme from 'enzyme';

test('renders Spinner', () => {
  const wrapper = enzyme.mount(<Spinner />);
  expect(wrapper.find('.loader')).toBe;
});
