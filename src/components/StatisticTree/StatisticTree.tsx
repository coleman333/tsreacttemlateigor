import * as React from 'react';
const { useCallback } = React;
import { useMappedState } from 'redux-react-hook';

import './StatisticTree.css';

export const StatisticTree = (): React.ReactElement => {
  const mapState = useCallback(
    state => ({
      equipmentStat: state.statistic.equipment,
      inventoryStat: state.statistic.inventory,
    }),
    [],
  );

  const { equipmentStat, inventoryStat } = useMappedState(mapState);

  return (
    <ul className='statistic-tree-list'>
      <li>
        Всього об'єктiв у системi: {
          equipmentStat.totalEquipment.count + inventoryStat.totalInventory.count || 0
        }
        <ul>
          <li>Обладнання: {equipmentStat.totalEquipment.count}</li>
          <li>
            ТМЦ: {inventoryStat.totalInventory.count}
            <ul>
              <li>
                ТМЦ у вiльному запасi:
                {
                  (inventoryStat.totalInventory.count - inventoryStat.mountedInventory.count) || 0
                }
              </li>
              <li>ТМЦ змонтовано: {inventoryStat.mountedInventory.count || 0}</li>
            </ul>
          </li>
        </ul>
      </li>
      <li>Промарковано ТМЦ: {inventoryStat.markedInventory.count || 0}</li>
    </ul>
  );
};
