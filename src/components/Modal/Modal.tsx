import * as React from 'react';
import * as ReactDOM from 'react-dom';

import './Modal.css';

const ModalWrapper = (props: ModalProps) => {

  const handleKeyDown = (e: any) => {
    if (e.keyCode === 27) {
      props.close();
    }
  };

  React.useEffect(() => {
    document.addEventListener('keydown', handleKeyDown);

    return (() => {
      document.removeEventListener('keydown', handleKeyDown);
    });
  });

  return (
    <div className='modal-wrapper'>
      {props.children}
    <span className='close-icon' onClick={props.close}>close</span>
  </div>
  );
};

interface ModalProps {
  children?: any;
  close: () => void;
}

export const Modal = (props: ModalProps) => ReactDOM.createPortal(
  <ModalWrapper close={props.close}>
    <div className='modal-container'>
      {props.children}
    </div>
  </ModalWrapper>,
  document.getElementById('portal') || document.body,
);
