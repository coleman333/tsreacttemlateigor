import * as React from 'react';
const { useCallback } = React;

import { useMappedState } from 'redux-react-hook';
import { TableItems } from '../../store/types/table';

import './Table.css';

import { FilterSection } from './components/Filters';
import { Row } from './components/Row/Row';
import { PlaceHolder } from './components/PlaceHolder/PlaceHolder';
import { Pagination } from '../Pagination/Pagination';
import { SelectPagesCount } from '../SelectPagesCount/SelectPagesCount';

export const Table = (): React.ReactElement => {
  const mapState = useCallback(
    state => ({
      data: (state.activeComponent === 'equipment')
        ? state.equipment.sorted.data
        : (state.activeComponent === 'inventory')
          ? state.inventory.sorted.data
          : null,
      activeFields: (state.activeComponent === 'equipment')
        ? state.equipment.activeFields
        : (state.activeComponent === 'inventory')
          ? state.inventory.activeFields
          : null,
      activeComponent: state.activeComponent,
    }),
    [],
  );

  const { data, activeFields, activeComponent } = useMappedState(mapState);

  let isEmpty = false;

  if (
    activeFields
    && (Object.keys(activeFields).length !== 0)
  ) {
    isEmpty = true;

    Object.keys(activeFields).map((key) => {
      if (activeFields[key].status === true) {
        isEmpty = false;
      }
    });
  }

  return (
    <React.Fragment>
      <h2 className='table-header'>
        {(activeComponent === 'inventory') && 'ТМЦ'}
        {(activeComponent === 'equipment') && 'Обладнання'}
      </h2>
      <div className='table-wrapper'>
        {
          activeFields &&
          (Object.keys(activeFields).length !== 0) &&
          <table className='table-container'>
            <thead>
              <tr>
                {
                  Object.keys(activeFields).map((item, idx) => {
                    if (activeFields[item]['status']) {
                      return (
                        <th key={idx}>
                          <div>{activeFields[item].name}</div>
                        </th>
                      );
                    }
                    {
                      return null;
                    }
                  })
                }
              </tr>

              {activeFields && <FilterSection />}
            </thead>
            <tbody>
              {
                (data && data.length !== 0)
                && data.map((items: TableItems, idx: number) => {
                  return (
                    <Row key={idx} data={items} activeFields={activeFields}/>
                  );
                })
              }
            </tbody>
          </table>
        }
        {isEmpty && <PlaceHolder />}
      </div>

      <div className='controll-page-menu'>
        <SelectPagesCount />
        <Pagination />
      </div>
    </React.Fragment>
  );
};
