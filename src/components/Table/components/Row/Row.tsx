import * as React from 'react';

export const Row = (props: any) => {
  const { data, activeFields } = props;

  return (
    <tr>
      {
          activeFields &&
          Object.keys(activeFields) &&
          (Object.keys(activeFields).length !== 0) &&
          Object.keys(activeFields).map((_item, _idx) => {
            if (_item && activeFields[_item].status) {
              if (activeFields[_item].filter.filterType === 'boolean') {
                return (
                  <td key={_idx}>
                    {data[_item] ? 'так' : 'нi'}
                  </td>
                );
              }
              {
                return (
                  <td key={_idx}>
                    {data[_item] || '-'}
                  </td>
                );
              }
            }
            {
              return null;
            }
          })
      }
    </tr>
  );
};
