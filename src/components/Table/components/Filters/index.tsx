import * as React from 'react';
const { useCallback } = React;

import { SelectFilter } from './SelectFilter/SelectFilter';
import { InputFilter } from './InputFilter/InputFilter';
import { DateFilter } from './DateFilter/DateFilter';
import { useMappedState } from 'redux-react-hook';

export const FilterSection = (): React.ReactElement => {

  const mapState = useCallback(
    state => ({
      activeFields: (state.activeComponent === 'equipment')
        ? state.equipment.activeFields
        : (state.activeComponent === 'inventory')
          ? state.inventory.activeFields
          : null,
    }),
    [],
  );

  const { activeFields } = useMappedState(mapState);

  return (
    <tr>
      {
        Object.keys(activeFields) &&
        (Object.keys(activeFields).length !== 0) &&
        Object.keys(activeFields).map((item, idx) => {
          if (activeFields[item]['status']) {
            return (
              <td key={idx} className='table-container-search-cell'>
                {
                  activeFields[item]['filter']['filterType'] === 'boolean' &&
                  <SelectFilter item={item} />
                }
                {
                  activeFields[item]['filter']['filterType'] === 'string' &&
                  <InputFilter item={item}/>
                }
                {
                  activeFields[item]['filter']['filterType'] === 'date' &&
                  <DateFilter item={item} />
                }
              </td>
            );
          }
          {
            return null;
          }
        })
      }
    </tr>
  );
};
