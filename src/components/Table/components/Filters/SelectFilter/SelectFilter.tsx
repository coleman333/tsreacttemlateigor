import * as React from 'react';
const { useState, useCallback } = React;
import { useDispatch, useMappedState } from 'redux-react-hook';

import { filterActionTypes } from '../../../../../store/types/actionTypes';

const textValues = {
  is_marked: {
    _: {
      name: 'Всi',
      value: '',
    },
    _true: {
      name: 'Промаркованi',
      value: 'true',
    },
    _false: {
      name: 'Непромаркованi',
      value: 'false',
    },
  },
};

export const SelectFilter = (props:{item: string}): React.ReactElement => {
  const { item } = props;
  const values = textValues[item];
  const [isMarked, setIsMarked] = useState(values[0]);

  const mapState = useCallback(
    state => ({
      activeComponent: state.activeComponent,
    }),
    [],
  );

  const { activeComponent } = useMappedState(mapState);

  const dispatch = useDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const name = e.target.value;
    setIsMarked(name);

    Object.keys(values).map((item:any) => {
      if ((values[item].name) === name) {
        dispatch({
          type: filterActionTypes[activeComponent],
          payload: {
            field: props.item,
            status:  true,
            filter: (values[item].value === 'true')
                    ? true
                    : (values[item].value === 'false')
                      ? false
                      : '',
          },
        });
      }
    });
  };

  return (
    <select value={isMarked} onChange={handleChange}>
      <option disabled={true}>Оберiть стан</option>
      {Object.keys(values).map((value, idx) => {
        return (
          <option key={idx} value={values[value].name}>
            {values[value].name}
          </option>
        );
      })}
    </select>
  );
};
