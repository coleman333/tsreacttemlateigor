import * as React from 'react';
const { useState } = React;

import './CustomSelect.css';

type CustomSelectProps = {
  values: any[],
  change: (val: string | number) => void,
  item: string,
  activeItem: string,
  chooseActive: (item: string) => void,
};

const prepareData = (data: any[]) => {
  if (data.length === 0) {
    return [];
  }

  const newData = data.map((value) => {
    if (typeof value === 'object') {
      return value['name'];
    }

    return value;
  });

  return newData;
};

export const CustomSelect = (props: CustomSelectProps) => {
  const { values, change, item, activeItem, chooseActive } = props;

  const [selected, setSelected] = useState<number | string>(item);
  const [filter, setFilter] = useState('');

  const handleChoose = (value: any): void => {
    if (isNaN(parseFloat(value)) || !isFinite(value)) {
      values.forEach((key, idx) => {
        if (key['name'] === value) {
          setSelected(value);
          change(idx + 1);
        }
      });
    } else {
      const _values = values.filter(_value => _value.toString().includes(value));

      setSelected(_values[0]);
      change(_values[0]);
    }

    setFilter('');
  };

  const handleClick = (e:any): void => {
    if (e.target.className === 'select-input') return;
    chooseActive(item);
  };

  const handleKeyDown = (e: any): void => {
    if (e.keyCode === 27) {
      setFilter('');
      chooseActive('');
    }

    if (e.keyCode === 13) {
      handleChoose(e.target.value);
      chooseActive('');
    }
  };

  const handleBlur = (e: any): void => {
    if (!e.relatedTarget || (e.relatedTarget.className !== 'select-value')) {
      chooseActive('');
      setFilter('');
    }
  };

  const selectValuesClassName = (item === activeItem) ? 'select-values active' : 'select-values';
  const data = prepareData(values);
  const filteredData = data.filter(item => item.toString().toLowerCase().includes(filter));

  return (
    <div className='select-wrapper' onClick={e => handleClick(e)}>
      <div className='selected-value'>
        {
          (item !== activeItem)
            ? <span className='selected'>{selected}</span>
            : <input
              className='select-input'
              autoFocus={true}
              value={filter}
              type='text'
              onChange={e => setFilter(e.target.value)}
              onKeyDown={e => handleKeyDown(e)}
              onBlur={e =>  handleBlur(e)}
            />
        }
        <span className='selected-icon'>&#10145;</span>
      </div>
      <div className={selectValuesClassName} >
        {
          filteredData.map((value, idx) => {
            return (
              <span
                tabIndex={-1}
                className='select-value'
                key={idx}
                onClick={() => handleChoose(value)}
              >
                  {value}
              </span>
            );
          })
        }
      </div>
    </div>
  );
};
