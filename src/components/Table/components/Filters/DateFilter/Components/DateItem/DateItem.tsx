import * as React from 'react';
const { useState } = React;

import './DateItem.css';

import { Date } from '../../DateFilter';
import { deepEquals } from '../../../../../../../helpers/deepEquals';
type DateItemProps = {
  date: Date | null;
  open: (item: string) => void;
  item: string;
};

export const DateItem = (props: DateItemProps) => {
  const [prevDate, setPrevDate] = useState<null | Date>(null);
  const [_date, set_date] = useState<null | string>(null);
  const { date, open, item } = props;

  if (!deepEquals(date, prevDate)) {
    setPrevDate(date);

    if (date) {
      set_date(prepareDate(date));
    } else {
      set_date(null);
    }
  }

  const handleClick = () => {
    open(item);
  };

  return (
    <span className='date-filter-item' onClick={handleClick}>
      {_date ? <pre>{item}: {_date}</pre> : <pre>{item}: 0000-00-00</pre>}
    </span>
  );
};

export const prepareDate = (date: Date) => {
  const _year = date.year;
  const _month = (
    (+date.month + 1).toString().length !== 1)
      ? (+date.month + 1)
      : (`0${+date.month + 1}`);
  const _day = (date.day.toString().length !== 1) ? date.day : (`0${date.day}`);

  return `${_year}-${_month}-${_day}`;
};
