import * as React from 'react';
const { useState, useEffect, useCallback } = React;

import './DateFilterModal.css';
import { CustomSelect } from '../CustomSelect/CustomSelect';
import { Date } from '../../DateFilter';

type DateFilterModalProps = {
  change: (date: Date) => void,
  close: () => void,
};

export const DateFilterModal = (props: DateFilterModalProps) => {
  const [wrapperClassName, setWrapperClassName] = useState('date-filter-modal-wrapper');
  const [currentYear, setCurrentYear] = useState('');
  const [currentMonth, setCurrentMonth] = useState('');
  const [currentDay, setCurrentDay] = useState('');
  const [active, setActive] = useState('');

  const { close, change } = props;

  const handleKeyDown = useCallback(
    (e) => {
      if (e.keyCode === 27) {
        if (!active) {
          handleClose();
        }
      }
    },
    [active],
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      return () => {
        document.removeEventListener('keydown', handleKeyDown);
      };
    },
    [active],
  );

  const reset = () => {
    setCurrentYear('');
    setCurrentMonth('');
    setCurrentDay('');
  };

  const years = [];
// tslint:disable-next-line: no-increment-decrement
  for (let i = 1990; i < 2040; i++) {
    years.push(i);
  }

  const months = [
    { name: 'January', days: 31 },
    { name: 'February', days: 28 },
    { name: 'March', days: 31 },
    { name: 'April', days: 30 },
    { name: 'May', days: 31 },
    { name: 'June', days: 30 },
    { name: 'July', days: 31 },
    { name: 'August', days: 31 },
    { name: 'September', days: 30 },
    { name: 'October', days: 31 },
    { name: 'November', days: 30 },
    { name: 'Desember', days: 31 },
  ];

  const days = [];

  const current = currentMonth ? months[+currentMonth - 1].days : 31;

// tslint:disable-next-line: no-increment-decrement
  for (let i = 0; i < current; i++) {
    days.push(i + 1);
  }

  const handleClose = async () => {
    setWrapperClassName('date-filter-modal-wrapper closing');
    setActive('');

    setTimeout(() => {
      reset();
      close();
    },         400);
  };

  if (currentYear && currentMonth && currentDay) {
    const date = {
      year: currentYear,
      month: +currentMonth - 1,
      day: currentDay,
    };

    change(date);
    reset();
    handleClose();
  }

  const chooseActive = (item: string) => {
    if (item === active) {
      setActive('');
    } else {
      setActive(item);
    }
  };

  const handleSetYear = (val: string) => {
    setCurrentYear(val);
  };

  const handleSetMonth = (val: string) => {
    setCurrentMonth(val);
  };

  const handleSetDay = (val: string) => {
    setCurrentDay(val);
  };

  return (
// tslint:disable: jsx-no-lambda
    <div className={wrapperClassName}>
      <CustomSelect
        activeItem={active}
        chooseActive={chooseActive}
        item={'year'}
        values={years}
        change={handleSetYear}
      />
      <CustomSelect
        activeItem={active}
        chooseActive={chooseActive}
        item={'month'}
        values={months}
        change={handleSetMonth}
      />
      <CustomSelect
        activeItem={active}
        chooseActive={chooseActive}
        item={'day'}
        values={days}
        change={handleSetDay}
      />
      <span className='date-filter-modal-close-icon' onClick={() => handleClose()}>&#10005;</span>
    </div>
// tslint:enable: jsx-no-lambda
  );
};
