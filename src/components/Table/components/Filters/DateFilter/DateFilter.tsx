import * as React from 'react';
const { useState, useCallback } = React;

import './DateFilter.css';

import { DateFilterModal } from './Components/DateFilterModal/DateFilterModal';
import { DateItem } from './Components/DateItem/DateItem';
import { useDispatch, useMappedState } from 'redux-react-hook';
import { deepEquals } from '../../../../../helpers/deepEquals';

import { filterActionTypes } from '../../../../../store/types/actionTypes';

export type Date = {
  year: string | number,
  month: string | number,
  day: string | number,
};

export const DateFilter = (props: any): React.ReactElement => {
  const [isOpenedModal, setIsOpenedModal] = useState(false);
  const [activeItem, setActiveItem] = useState('');
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [prevStateDates, setPrevStateDates] = useState<null | Object>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);

  const mapState = useCallback(
    state => ({
      activeComponent: state.activeComponent,
    }),
    [],
  );

  const { activeComponent } = useMappedState(mapState);

  const dispatch = useDispatch();

  const resetDateState = () => {
    setStartDate(null);
    setEndDate(null);
  };

  const handleChangeValue = (val: Date) => {
    if (activeItem === 'from') {
      setStartDate(val);
    }

    if (activeItem === 'to') {
      setEndDate(val);
    }
  };

  const handleOpenModal = (item: string) => {
    setActiveItem(item);
    setIsOpenedModal(true);
  };

  const handleCloseModal = () => {
    setIsOpenedModal(false);
  };

  if (!deepEquals(prevStateDates, { startDate, endDate })) {
    setPrevStateDates({ startDate, endDate });

    let filter = '';
    if (startDate && endDate) {
      const start = new Date(+startDate.year, +startDate.month, +startDate.day).getTime();
      const end = new Date(+endDate.year, +endDate.month, +endDate.day).getTime();

      filter = `[${start}, ${end}]`;
    } else {
      filter = '';
    }

    dispatch({
      type: filterActionTypes[activeComponent],
      payload: {
        filter,
        field: props.item,
        status:  true,
      },
    });
  }

  if (true) {
    return (
      <div className='date-filter-container'>
        <div className='date-filter-wrapper'>
          <DateItem
            item='from'
            date={startDate}
            open={handleOpenModal}
          />
          <DateItem
            item='to'
            date={endDate}
            open={handleOpenModal}
          />
        </div>
        <span onClick={resetDateState}>&#10005;</span>
      {
        isOpenedModal &&
          <DateFilterModal
            close={handleCloseModal}
            change={handleChangeValue}
          />
      }
      </div>
    );
  }
};
