import * as React from 'react';
const { useState, useCallback, useRef, useEffect } = React;
import { useDispatch, useMappedState } from 'redux-react-hook';

import './InputFilter.css';

import { filterActionTypes } from '../../../../../store/types/actionTypes';

export const InputFilter = (props: any): React.ReactElement => {
  const [name, setName] = useState('');
  const nameRef = useRef<any>();

  const mapState = useCallback(
    state => ({
      activeComponent: state.activeComponent,
      activeFields: state.activeComponent
                    && state[state.activeComponent]
                    && state[state.activeComponent].activeFields,
    }),
    [],
  );

  useEffect(
    () => {
      return () => {
        dispatch({
          type: filterActionTypes[activeComponent],
          payload: {
            field: props.item,
            filter: '',
          },
        });
      };
    },
    [],
  );

  const { activeComponent, activeFields } = useMappedState(mapState);

  const dispatch = useDispatch();

  const handleClear = (e:React.MouseEvent) => {
    handleChange();
    nameRef.current.focus();
  };

  const handleChange = (e?: React.ChangeEvent<HTMLInputElement>) => {
    let value;

    if (e === undefined) {
      value = '';
    } else {
      value = e.target.value;
    }

    if (activeFields[props.item].filter.value === value) {
      return;
    }

    setName(value.trim());

    dispatch({
      type: filterActionTypes[activeComponent],
      payload: {
        field: props.item,
        status:  true,
        filter: value.trim(),
      },
    });
  };

  return (
    <div className='input-filter-wrapper'>
      <input
        ref={nameRef}
        type='text'
        value={name}
        placeholder='введiть значення'
        onChange={handleChange}
      />
      <span onClick={handleClear}>&#10005;</span>
    </div>
  );
};
