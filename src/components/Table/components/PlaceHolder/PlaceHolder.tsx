import * as React from 'react';

import './PlaceHolder.css';

export const PlaceHolder = (): React.ReactElement => {
  return (
    <div className='table-no-rows-selected-placeholder'>
      Виберiть хоча б одну позицiю
    </div>
  );
};
