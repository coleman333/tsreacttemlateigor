import * as React from 'react';
const { useState, useCallback } = React;
import { useMappedState, useDispatch } from 'redux-react-hook';

import './SelectColumns.css';

import { tableActionTypes } from '../../store/types/actionTypes';
import { tableHeadersNames, tableTypes } from '../../store/types/table';

export const SelectColumns = () => {
  const [equipmentInit, setEquipmentInit] = useState(false);
  const [inventoryInit, setInventoryInit] = useState(false);

  const dispatch = useDispatch();

  const mapState = useCallback(
    state => ({
      activeComponent: state.activeComponent,
      inventory: state.inventory.sorted.data,
      equipment: state.equipment.sorted.data,
      activeInventoryFields: state.inventory.activeFields,
      activeEquipmentFields: state.equipment.activeFields,
    }),
    [],
  );

  const {
    activeComponent,
    inventory,
    equipment,
    activeInventoryFields,
    activeEquipmentFields,
  } = useMappedState(mapState);

  if (!equipmentInit) {
    if (activeComponent && activeComponent === 'equipment') {
      if (equipment) {
        const list: Object[] = [];

        for (const key in equipment[0]) {
          if (tableHeadersNames[key]) {
            list.push({
              key,
              name: tableHeadersNames[key],
              filterType: tableTypes[key],
            });
          }
        }

        dispatch({
          type: tableActionTypes.setAllEquipment,
          payload: {
            data: list,
          },
        });

        setEquipmentInit(true);
      }
    }
  }

  if (!inventoryInit) {
    if (activeComponent && (activeComponent === 'inventory')) {
      if (inventory) {
        const list: Object[] = [];

        for (const key in inventory[0]) {
          if (tableHeadersNames[key]) {
            list.push({
              key,
              name: tableHeadersNames[key],
              filterType: tableTypes[key],
            });
          }
        }

        dispatch({
          type: tableActionTypes.setAllInventory,
          payload: { data: list },
        });

        setInventoryInit(true);
      }
    }
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>, item: string): void => {
    const type = (activeComponent === 'inventory')
      ? tableActionTypes.setInventory
      : tableActionTypes.setEquipment;

    dispatch({
      type,
      payload: {
        key: item,
        value: e.target.checked,
      },
    });
  };

  const activeFields = (activeComponent === 'inventory')
    ? activeInventoryFields
    : activeEquipmentFields;

  const tableHeaders:string[] = [];

  for (const i in activeFields) {
    if (tableHeadersNames[i]) {
      if (!tableHeaders.includes(activeFields[i])) {
        tableHeaders.push(tableHeadersNames[i]);
      }
    }
  }

  const columnList = Object.keys(activeFields).map((item, idx) => {
    if (tableHeaders[idx]) {
      return (
        <li key={item} className='select-table-colums-item-container'>
          <label className='select-table-colums-item'>
            <span>{tableHeaders[idx]}</span>
            <input
              type="checkbox"
              checked={activeFields[item].status}
// tslint:disable-next-line: jsx-no-lambda
              onChange={e => handleChange(e, item)}
            />
            <span className='select-table-colums-checkmark'/>
          </label>
        </li>
      );
    }
    {
      return null;
    }
  });

  if (activeFields && Object.keys(activeFields).length !== 0) {
    return (
      <div className="select-table-colums-wrapper">
        <ul className='select-table-colums-list'> {columnList} </ul>
      </div>
    );
  }

  return null;
};
