import * as React from 'react';
const { useState, useCallback } = React;
import { useMappedState, useDispatch } from 'redux-react-hook';

import { SwitchBtn } from './SwitchBtn/SwitchBtn';
import { pagesActionTypes } from '../../store/types/actionTypes';
import helpers from '../../helpers';

import './Pagination.css';

const maxAllowedPages = 5;

export const Pagination = () => {
  const [itemsTotal, setItemsTotal] = useState(0);
  const [prevItemsPerPage, setPrevItemsPerPage] = useState(0);
  const [prevActivePage, setPrevActivePage] = useState(0);
  const [prevActiveComponent, setPrevActiveComponent] = useState(null);
  const dispatch = useDispatch();

  const mapState = useCallback(
    state => ({
      itemsPerPage: state.pagination.itemsPerPage,
      activeComponent: state.activeComponent,
      distinct: state.pagination[`distinct${helpers.capitalize(state.activeComponent)}`],
      activePage: state.pagination.activePage,
    }),
    [],
  );

  const {
    itemsPerPage,
    distinct,
    activeComponent,
    activePage,
  } = useMappedState(mapState);

  if (!helpers.deepEquals(prevActivePage, activePage)) {
    setPrevActivePage(activePage);
  }

  if (distinct['status'] !== "fetching") {
    if (!helpers.deepEquals(prevActiveComponent, activeComponent)) {
      setPrevActiveComponent(helpers.deepCopy(activeComponent));

      dispatch({
        type: pagesActionTypes.setActivePage,
        payload: { activePage: 0 },
      });
    }

    if (!helpers.deepEquals(prevItemsPerPage, itemsPerPage)) {
      setPrevItemsPerPage(itemsPerPage);

      dispatch({
        type: pagesActionTypes.setActivePage,
        payload: {
          activePage:
          (((Math.ceil(activePage * prevItemsPerPage / itemsPerPage)) > 0)
            ? (Math.floor(activePage * prevItemsPerPage / itemsPerPage))
            : (Math.floor(activePage * itemsPerPage / prevItemsPerPage))) || 0,
        },
      });
    }

    if ((itemsTotal !== distinct['count'])) {
      // e.g.: 'getInventoryDistinctAsync' or 'getEquipmentDistinctAsync'
      const _type = `get${helpers.capitalize(activeComponent)}DistinctAsync`;

      // pagination saga
      dispatch({ type: pagesActionTypes[_type] });

      setItemsTotal(distinct['count']);
    }
  }

  const totalPages = Math.ceil(itemsTotal / itemsPerPage);

  const changePage = (i: string | number) => {
    if (i === 'undefined' || i === null) return;
    if (i >= totalPages) return;

    dispatch({
      type: pagesActionTypes.setActivePage,
      payload: { activePage: +i },
    });
  };

  const pages = [];

  if (totalPages < maxAllowedPages) {
    // if pages less than maxAllowedPages
// tslint:disable-next-line: no-increment-decrement
    for (let i = 0; i < totalPages; i++) {
      const classname = (i !== activePage)
      ? 'paging-single-page-unit'
      : 'paging-single-page-unit paging-single-page-unit-selected';

      pages.push(
        <SwitchBtn
          key={i}
          className={classname}
          i={i}
          changePage={changePage}
          name={i + 1}
        />,
      );
    }
  } else {
    // if pages more than maxAllowedPages
// tslint:disable-next-line: no-increment-decrement
    for (let i = 0; i <= totalPages; i++) {
      const classname = (i !== activePage)
        ? 'paging-single-page-unit'
        : 'paging-single-page-unit paging-single-page-unit-selected';

      if (activePage === 0) {
        // if first page selected
        if (i <= 2) {
          pages.push(
            <SwitchBtn
              key={i}
              className={classname}
              i={i}
              changePage={changePage}
              name={i + 1}
            />,
          );
        }
      } else if (activePage === totalPages - 1) {
        // if last page selected
        if ((i >= totalPages - 3) && (i !== totalPages)) {
          const classname = (i !== activePage)
            ? 'paging-single-page-unit'
            : 'paging-single-page-unit paging-single-page-unit-selected';

          pages.push(
            <SwitchBtn
              key={i}
              className={classname}
              i={i}
              changePage={changePage}
              name={i + 1}
            />,
          );
        }
      } else {
        // if not first and not last page selected
        if ((i >= activePage - 1) && (i <= activePage + 1)) {
          const classname = (i !== activePage)
            ? 'paging-single-page-unit'
            : 'paging-single-page-unit paging-single-page-unit-selected';

          pages.push(
            <SwitchBtn
              key={i}
              className={classname}
              i={i}
              changePage={changePage}
              name={i + 1}
            />,
          );
        }
      }
    }
  }

  const paging = (
    <nav className='paging-wrapper'>
      <SwitchBtn
// tslint:disable-next-line: jsx-no-lambda jsx-no-multiline-js
        changePage={() => changePage(0)}
        name='first'
      />
      <SwitchBtn
// tslint:disable-next-line: jsx-no-lambda jsx-no-multiline-js
        changePage={() => changePage(
          (activePage - 1) >= 0
          ? (activePage - 1)
          : (activePage),
        )}
        name='prev'
      />

        {pages}

      <SwitchBtn
// tslint:disable-next-line: jsx-no-lambda jsx-no-multiline-js
        changePage={() => changePage(
          (activePage + 1) <= (totalPages)
          ? (activePage + 1)
          : totalPages,
        )}
        name='next'
      />
      <SwitchBtn
// tslint:disable-next-line: jsx-no-lambda jsx-no-multiline-js
        changePage={() => changePage(totalPages - 1)}
        name='last'
      />
    </nav>
  );

  return paging;
};
