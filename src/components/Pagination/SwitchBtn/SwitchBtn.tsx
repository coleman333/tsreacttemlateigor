import * as React from 'react';

type SwitchBtnProps = {
  changePage: Function,
  className?: string,
  name: string | number,
  i? :number,
};

export const SwitchBtn = (props:SwitchBtnProps) => {
  const { changePage, className, name, i } = props;

  const handleKeyPress = (e:React.KeyboardEvent): void => {
    if (e.keyCode === 13) {
      changePage(i);
    }
  };

  const handleClick = () => {
    changePage(i);
  };

  return (
    <div
      className={className || 'paging-single-page-unit'}
      tabIndex={0}
      onClick={handleClick}
      onKeyDown={handleKeyPress}
    >
        <span>{name}</span>
    </div>
  );
};
