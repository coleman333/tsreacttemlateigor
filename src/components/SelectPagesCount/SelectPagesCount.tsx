import * as React from 'react';
const { useState } = React;

import { useDispatch } from 'redux-react-hook';

import { pagesActionTypes } from '../../store/types/actionTypes';

import './SelectPagesCount.css';

const itemsPerPageArray = [
  10, 20, 25, 50, 100,
];

export const SelectPagesCount = (): React.ReactElement => {
  const [value, setValue] = useState(10);
  const dispatch = useDispatch();

  const handleChange = (_value: string | number): void => {
    dispatch({
      type: pagesActionTypes.setPageCount,
      payload: { itemsPerPage: +_value },
    });
    setValue(+_value);
  };

  const renderedItems = itemsPerPageArray.map((item, idx) => {
    return (
      <option key={idx} value={item}>
        {item}
      </option>
    );
  });

  return (
    <div className='select-pages-count-wrapper'>
      <span>Кiлькiсть одиниць на сторiнцi: </span>
      {/* tslint:disable-next-line: jsx-no-lambda */}
      <select value={value} onChange={e => handleChange(e.target.value)}>
        {renderedItems}
      </select>
    </div>
  );
};
