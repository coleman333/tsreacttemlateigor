import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { StoreContext } from 'redux-react-hook';
import { BrowserRouter } from 'react-router-dom';

import './index.css';

import { store } from './store';

import { App } from './App';

window.addEventListener('resize', () => {
  console.log(window.innerHeight);
});

ReactDOM.render(
  <StoreContext.Provider value={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </StoreContext.Provider>,
  document.getElementById('root'),
);
